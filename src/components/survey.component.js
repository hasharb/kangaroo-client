import React, { Component } from "react";
import _map from 'lodash/map'
import { Redirect } from "react-router-dom";
import AuthService from "../services/auth.service";
import SurveyService from "../services/survey.service";

export default class Survey extends Component {

    constructor(props) {
        super(props);
        this.state = {
            code: "",
            data: []
        }
    }

    getData() {
        SurveyService.getAggregatedResults(this.state.code)
            .then(json => {
                console.log(json.data.data)
                this.setState({
                    ...this.state,
                    data: json.data.data
                });
            });
    }

    componentDidMount() {
        this.getData()
    }

    filterSurveys(code) {
        this.state.code = code;
        this.getData();
    }

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.state.code = e.target.value;
            this.getData();
        }
    }

    renderAnswers(result) {
        if (typeof (result) == "object")
            result = Object.keys(result).map((key) => [key, result[key]]);

        if (Array.isArray(result)) {
            return (<div>{result.map((res, i) => {
                return (<p>{res[0]} : {res[1]}</p>)
            })}</div>
            )
        } else {
            return (<p>{result}</p>)
        }
    }

    render() {
        return (
            <>
                <div>
                    Search: <input onKeyPress={ this.handleKeyPress} />
                    {this.state.data.map((item, index) => {
                        return (<div>
                            <br /><h2 onClick={() => this.filterSurveys(item.code)} id={item.code}><b>{++index}. {item.name}</b></h2><br />
                            {item.survey_results.map((results, i) => {
                                return (<div>
                                    <p><b>{results.label}</b></p>
                                    {this.renderAnswers(results.result)}
                                </div>)
                            })}
                        </div>
                        )
                    })}
                </div>

            </>
        )
    }
}