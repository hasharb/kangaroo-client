import React, { Component } from "react";
 
export default class ThankYouPage extends Component {
  render() {
    return (
        <div className="row justify-content-center">
        <div className="col-10 col-sm-7 col-md-5 col-lg-4">
          <p>
            Registration successful.
          </p>
          <p>
            Thanks for signing up!
          </p>
        </div>
      </div>
    );
  }
}