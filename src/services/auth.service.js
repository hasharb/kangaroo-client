import axios from "axios";

const API_URL = "http://localhost:8080/api/auth/";

class AuthService {

  login(email, password) {
    return axios
      .post(API_URL + "login", {
        email,
        password
      })
      .then(response => {
        if (response.data.access_token) {
          localStorage.setItem("user", JSON.stringify(response.data.access_token));
        }

        return response.data.access_token;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }

  register(name,email, password,password_confirmation) {
    return axios.post(API_URL + "register", {
      name,
      email,
      password,
      password_confirmation
    });
  }
}

export default new AuthService();
