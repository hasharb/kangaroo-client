import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8080/api/survey/';

class SurveyService {
  getAggregatedResults(code) {
    var url = API_URL + "aggregate";
    if(code != '') url += "?code="+code;
    return axios.get(url
        // ,{ headers: authHeader() }
        );
  }
}

export default new SurveyService();